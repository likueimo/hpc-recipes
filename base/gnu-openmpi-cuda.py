#!/usr/bin/env python
########
#  HPCCM script for CUDA-Aware MPI (Open MPI)
#  update : 2020/06/20
#  
#  host
#  --------------------------------
#  CentOS 7
#  mlnx_ofed 5 (with legacy library)
#  gdrcopy 2.0
#  knem 1.1.3
#  slurm/pmix 18.08.8/2.2.2
#  --------------------------------
#
#  container 
#  --------------------------------
#  mlnx_ofed 4 default legacy library
cntr_mlnx_ofed = '4.7-3.2.9.0' 

#  below version should be same as host
cntr_gdrcopy = '2.0'
cntr_knem = '1.1.3'
cntr_slurm_pmi2 = '18.08.8'

#　if no API compatibility issue
#  ucx, openmpi, the newer the better
cntr_ucx = '1.8.0'
cntr_openmpi = '4.0.4'

#  TO DO :
#  enhance pmix compatibility host slurm plugin <-> guest 
#  From Mellanox HPC-X docs :
#  intended to be used with SLURM PMIx plugin, 
#  Open MPI should be build against \
#  external PMIx, Libevent and HWLOC \
#  and the same Libevent and PMIx libraries \
#  should be used for both SLURM and Open MPI
#  --------------------------------
########

import hpccm

# Use appropriate container base images based on the CPU architecture
arch = 'x86_64'
default_build_image = 'nvidia/cuda:10.1-devel-ubuntu18.04'
default_runtime_image = 'nvidia/cuda:10.1-base-ubuntu18.04'

########
# Build stage (Stage 0)
########

# Base image
Stage0 += baseimage(image=default_build_image,_arch=arch, _as='build')

# replace mirror site to NCHC free software labs
#Stage0 += raw(singularity="sed -i 's#archive.ubuntu.com#free.nchc.org.tw#' /etc/apt/sources.list")
#Stage0 += raw(docker="sed -i 's#archive.ubuntu.com#free.nchc.org.tw#' /etc/apt/sources.list")

# enbale gfortran
Stage0 += gnu(fortran=True)

# OpenMPI Communication stack:
#
# OpenMPI 4 (built in pmix 3 -> enable 'srun --mpi=pmix')
# with
# Slurm PMI2 (enable 'srun --mpi=pmi2')
# UCX (with KNEM + GDRCOPY + Mellanox OFED)

Stage0 += mlnx_ofed(version=cntr_mlnx_ofed)

Stage0 += gdrcopy(ldconfig=True, version=cntr_gdrcopy)

Stage0 += knem(ldconfig=True, version=cntr_knem)

Stage0 += ucx(
              ofed=True,
              without_java=True,
              enable_devel_headers=True,
              ldconfig=True,
              version=cntr_ucx,
              knem='/usr/local/knem',
              gdrcopy='/usr/local/gdrcopy',
             )

# with Slurm PMI2
Stage0 += slurm_pmi2(prefix='/usr/local/slurm-pmi2', version=cntr_slurm_pmi2)

mpi = openmpi(
              cuda=True,
# becasue of https://www.open-mpi.org/faq/?category=openfabrics#ofa-device-error
# need to disable verbs for openmpi-cuda with ucx -> then infiniband=False
              infiniband=False,
              without_xpmem=True,
              enable_mpi_cxx=True,
              enable_mpi_fortran=True,
              enable_mpi1_compatibility=True,
              ldconfig=True,
              version=cntr_openmpi,
              ucx='/usr/local/ucx',
              with_pmi='/usr/local/slurm-pmi2',
              with_platform='contrib/platform/mellanox/optimized',
             )

Stage0 += mpi

########
# OpenMPI envs
########
Stage0 += environment(variables={
# enable openmpi-cuda Point-to-point management layer(pml) over ucx-cuda
# ref : https://www.open-mpi.org/faq/?category=runcuda#run-ompi-cuda-ucx
                                 'OMPI_MCA_pml': 'ucx',
                                 'UCX_TLS': 'rc,sm,cuda_copy,gdr_copy,cuda_ipc',
# for avoid this ucx-cuda issue
# ref : https://github.com/openucx/ucx/wiki/NVIDIA-GPU-Support
                                 'UCX_MEMTYPE_CACHE': 'n'
                                })
