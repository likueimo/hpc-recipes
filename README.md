# TWCC HPC SINGULARITY  
 [[_TOC_]]

- 注意: 此專案還在測試階段  

提供適用 TWCC HPC 的 singularity 範本 (目前範本以 OpenMPI-CUDA 為主)  
可以基於提供的範本，透過 HPC Container Maker (HPCCM) 堆疊自己的 MPI 應用程式，  
當 HOST 系統有任何變動，可以快速重編應用程式的 singularity 容器。   
主要啟發以及參考這篇 [NVIDIA 部落格文章](https://devblogs.nvidia.com/building-hpc-containers-demystified)。  

- THIS IS UNDER CONSTRUCTION  

HPC Container Maker (HPCCM) script for twcc singularity container   
Inspired by [this NVIDIA blog post](https://devblogs.nvidia.com/building-hpc-containers-demystified)  

# USAGE 

建議在虛擬機環境執行。

Recommended to execute this steps inside a virtual machine.  

## 1. Install HPCCM and Singularity  
- [HPC Container Maker(HPCCM)](https://github.com/NVIDIA/hpc-container-maker) >= 19.11   
    
    install command with example:
    ```
    sudo pip install hpccm  
    ```

- [Singularity](https://sylabs.io/singularity) >= 3.2  
    
    install command with Debian/Ubuntu example:
    ```
    sudo wget -O- http://neuro.debian.net/lists/xenial.us-ca.full | sudo tee /etc/apt/sources.list.d/neurodebian.sources.list && \
    sudo apt-key adv --recv-keys --keyserver hkp://pool.sks-keyservers.net:80 0xA5D32F012649A5A9 && \
    sudo apt-get update
    sudo apt-get install -y singularity-container
    ```
    install command with CentOS/RHEL example:
    ```
    sudo yum update -y && \
    sudo yum install -y epel-release && \
    sudo yum update -y && \
    sudo yum install -y singularity-runtime singularity
    ```


## 2. Build MPI compiler dependent apps Container  
搭建 MPI compiler 相依的應用程式容器

### 2.0 Base script
#### gnu-openmpi-cuda.py
optimized OpenMPI-CUDA singularity recipe for TWCC   
(If your apps need NCCL, this is NOT suitable)   

- ubuntu 18.04 / gnu 7 (default gnu version)
- cuda 10.1
- openmpi 4.0.3

#### gnu-mt-openmpi-cuda.py
- same as above, enable multi-thread

#### gnu-openmpi-nccl.py (TO DO)

#### pgi-openmpi-cuda.py (TO DO)


### 2.1 download base MPI compiler hpccm script (ex: gnu-openmpi-cuda.py)

下載腳本，以 gnu-openmpi-cuda.py 為例子
```
wget https://gitlab.com/likueimo/twcc-hpc-singularity/-/raw/master/base/gnu-openmpi-cuda.py
```

### 2.2 edit 'gnu-openmpi-cuda.py' and add dependent apps.   

複製 gnu-openmpi-cuda.py，  
並加入要和 MPI compiler 一起編譯的應用程式  
```
cp gnu-openmpi-cuda.py gnu-openmpi-cuda-$apps-$maintainer.py
vim gnu-openmpi-cuda-$apps-$maintainer.py (add your apps script)
```

### 2.3 produce Singularity Definition File
透過 HPCCM 提供的 cli，產生 Singularity Definition File (相當於 dockerfile)
```
hpccm --recipe gnu-openmpi-cuda-$apps-$maintainer.py --format singularity --singularity-version=3.2 > gnu-openmpi-cuda-$apps-$maintainer.def
```

### 2.4 build singularity container   
建立 singularity 容器，建議在虛擬機環境執行。
```
sudo singularity build gnu-openmpi-cuda-$apps-$maintainer.sif twcc-hpcbase-gnu-openmpi-cuda-$apps-$maintainer.def > twcc-hpcbase-gnu-openmpi-cuda-$apps-$maintainer.log 2>&1
```

## 3. see logs   
```
tail -f gnu-openmpi-cuda-$apps-$maintainer.log 
```
