# build hwloc

Stage0 += packages(apt=['autoconf', 'automake', 'libtool', 'libxml2-dev'])
Stage0 += generic_autotools(
                            preconfigure=['autoreconf --force --install'],
                            prefix='/usr/local/hwloc',
                            url='https://download.open-mpi.org/release/hwloc/v{}/hwloc-{}.tar.gz'.\
                                format(twcc_hwloc[:-2],twcc_hwloc)
                           )
Stage0 += environment(variables={'PATH': '/usr/local/hwloc/bin:$PATH',
                                 'LD_LIBRARY_PATH': '/usr/local/hwloc/lib:$LD_LIBRARY_PATH',
                                 'CPATH': '/usr/local/hwloc/include:$CPATH'})

