# TWCC LAMMPS container (alpha ver.)
[[_TOC_]]

利用 base 腳本 (gnu-openmpi-cuda.py) 為底，  
加上參考 NVIDIA 撰寫的 [LAMMPS 的 HPCCM 腳本](https://github.com/NVIDIA/hpc-container-maker/blob/master/recipes/lammps/lammps.py)，    
編譯出適用 TWCC 的 LAMMPS singularity container。  
由於 LAMMPS 編譯參數可以調整許多，編譯出來的 LAMMPS 的功能尚未驗證。  

THIS IS UNDER CONSTRUCTION !   

HPCCM script for twcc lammps singularity container   
modify from [NVIDIA HPCCM recipes](https://github.com/NVIDIA/hpc-container-maker/blob/master/recipes/lammps/lammps.py)  

singularity container already copy to tihs path  
/work/TWCC_cntr/gnu-openmpi-cuda-lammps-kmo.sif  
(you need to login ln01.twcc.ai)  

## files 
### gnu-openmpi-cuda-lammps-kmo.py   
 base on `gnu-openmpi-cuda.py` and add lammps.   
### gnu-openmpi-cuda-lammps-kmo.def  
 Singularity Definition File produced by HPCCM  
### gnu-openmpi-cuda-lammps-kmo.log   
 the log of building singularity container  
### srun_job_lammps.sh    
 simple slurm job script for TWCC HPC environment (Taiwania 2)  

## modify 
 You can modify `gnu-openmpi-cuda-lammps-kmo.py`   
 and re-produce Singularity Definition File  
```
hpccm --recipe gnu-openmpi-cuda-lammps-$maintainer.py --format singularity --singularity-version=3.2 > gnu-openmpi-cuda-lammps-$maintainer.def
```

## build singularity container  

```
sudo singularity build gnu-openmpi-cuda-lammps-$maintainer.sif gnu-openmpi-cuda-lammps-$maintainer.def > gnu-openmpi-cuda-lammps-$maintainer.log 2>&1
```

## submit slurm job with twcc lammps container
modify `srun_job_lammps.sh`  

and submit jobs  
`sbatch srun_job_lammps.sh`  
(you need to login ln01.twcc.ai)  
