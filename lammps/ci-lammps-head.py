#!/usr/bin/env python
import hpccm

# Use appropriate container base images based on the CPU architecture
arch = 'x86_64'
default_build_image = 'ggkmo/gnu-openmpi-cuda:latest'

########
# Build stage (Stage 0)
########

# Base image
Stage0 += baseimage(image=default_build_image, _arch=arch, _as='build')
