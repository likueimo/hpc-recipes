#!/usr/bin/env python
########
#  optimized OpenMPI-CUDA-lammps singularity recipe for TWCC
#  update : 2020/03/16
#
#  host : TWCC GPU NODE
#  --------------------------------
#  CentOS 7.5 x86_64
#  mlnx_ofed (4.4-2.0.7.0/4.4-2.0.7.0.3)
#  gdrcopy (2.0)
#  knem (1.1.3)
#  slurm/pmix (18.08.8/2.2.2)
#
#  TO DO (HOST):
#  xpmem (waiting for host install xpmem kernel module)
#  hcoll (waiting for host enable sharp service)
#  --------------------------------
#
#  guest : singularity container
#  --------------------------------
#  guest mlnx_ofed 4.x userspace driver,
#  should be compatible host mlnx_ofed 4.4
twcc_mlnx_ofed = '4.7-3.2.9.0'

#  guest gdrcopy/knem userspace library version, slurm pmi2 version,
#  have to same as host gdrcopy/knem/slurm version
twcc_gdrcopy = '2.0'
twcc_knem = '1.1.3'
twcc_slurm_pmi2 = '18.08.8'

#  ucx, the newer the better
twcc_ucx = '1.7.0'

#　openmpi, if no MPI API compatibility issue
#  the newer the better
twcc_openmpi = '4.0.3'

#  TO DO (GUEST) :
#  enhance pmix compatibility host slurm plugin <-> guest
#  From Mellanox HPC-X docs :
#  intended to be used with SLURM PMIx plugin,
#  Open MPI should be build against \
#  external PMIx, Libevent and HWLOC \
#  and the same Libevent and PMIx libraries \
#  should be used for both SLURM and Open MPI
#  --------------------------------
########
#  --------------------------------
#  apps
twcc_cmake = '3.16.5'
twcc_lammps = 'stable_3Mar2020'
#  --------------------------------



import hpccm

# Use appropriate container base images based on the CPU architecture
arch = 'x86_64'
default_build_image = 'nvidia/cuda:10.1-devel-ubuntu18.04'
default_runtime_image = 'nvidia/cuda:10.1-base-ubuntu18.04'

########
# Build stage (Stage 0)
########

# Base image
Stage0 += baseimage(image=USERARG.get('build_image', default_build_image),
                    _arch=arch, _as='build')

# replace mirror site to NCHC free software labs
Stage0 += raw(singularity="sed -i 's#archive.ubuntu.com/ubuntu/#free.nchc.org.tw/ubuntu#' /etc/apt/sources.list")

# enbale gfortran
Stage0 += gnu(fortran=True)

# OpenMPI Communication stack:
#
# OpenMPI 4 (built in pmix 3 -> enable 'srun --mpi=pmix')
# with
# Slurm PMI2 (enable 'srun --mpi=pmi2')
# UCX (with KNEM + GDRCOPY + Mellanox OFED)

Stage0 += mlnx_ofed(version=twcc_mlnx_ofed)

Stage0 += gdrcopy(ldconfig=True, version=twcc_gdrcopy)

Stage0 += knem(ldconfig=True, version=twcc_knem)

Stage0 += ucx(
              ofed=True,
              without_java=True,
              enable_devel_headers=True,
              ldconfig=True,
              version=twcc_ucx,
              knem='/usr/local/knem',
              #xpmem='' wait for host enable xpmem kernel module
              gdrcopy='/usr/local/gdrcopy',
             )

# with Slurm PMI2
Stage0 += slurm_pmi2(prefix='/usr/local/slurm-pmi2', version=twcc_slurm_pmi2)

mpi = openmpi(
              cuda=True,
# becasue of https://www.open-mpi.org/faq/?category=openfabrics#ofa-device-error
# need to disable verbs for openmpi-cuda with ucx -> then infiniband=False
              infiniband=False,
              #with_hcoll=True, wait for host enable sharp
              without_xpmem=True,
              enable_mpi_cxx=True,
              enable_mpi_fortran=True,
              enable_mpi1_compatibility=True,
              ldconfig=True,
              version=twcc_openmpi,
              ucx='/usr/local/ucx',
              with_pmi='/usr/local/slurm-pmi2',
              with_platform='contrib/platform/mellanox/optimized',
             )

Stage0 += mpi

# kmo try to build lammps
# base on https://github.com/NVIDIA/hpc-container-maker/blob/master/recipes/lammps/lammps.py
# need to verify
# -----------------------------------------------------------------------------------
########
# LAMMPS
########

# lammps build by cmake
Stage0 += cmake(eula=True, version=twcc_cmake)

gpu_arch = USERARG.get('gpu_arch', 'Volta70')

compute_capability = 'sm' + gpu_arch[-2:]
srcdir = '/var/tmp/lammps-{}'.format(twcc_lammps)

Stage0 += comment('LAMMPS version {0} for CUDA compute capability {1}'.format(
  twcc_lammps, compute_capability))

# LAMMPS dependencies
Stage0 += apt_get(ospackages=['bc', 'git', 'libgomp1', 'libhwloc-dev', 'make',
                              'tar', 'wget'])

# LAMMPS build
Stage0 += generic_cmake(
  build_directory='{0}/build-{1}'.format(srcdir, gpu_arch),
  cmake_opts=[
              # cmake
              '-D CUDA_USE_STATIC_CUDA_RUNTIME=OFF',
              '-D MPI_C_COMPILER={}'.format(mpi.toolchain.CC),
              # https://lammps.sandia.gov/doc/Build_cmake.html
              '-D CMAKE_BUILD_TYPE=Release',
              # https://lammps.sandia.gov/doc/Build_basics.html#serial
              '-D BUILD_MPI=yes',
              '-D BUILD_OMP=yes',
              # https://lammps.sandia.gov/doc/Build_basics.html#exe
              '-D BUILD_LIB=no',
              '-D BUILD_SHARED_LIBS=yes',
              # https://lammps.sandia.gov/doc/Build_extras.html#kokkos
              '-D KOKKOS_ARCH="{}"'.format(gpu_arch),
              '-D KOKKOS_ENABLE_CUDA=yes',
              '-D KOKKOS_ENABLE_OPENMP=yes',
              '-D KOKKOS_ENABLE_HWLOC=yes',
              '-D CMAKE_CXX_COMPILER={}/lib/kokkos/bin/nvcc_wrapper'.format(srcdir),
              # https://github.com/lammps/lammps/blob/master/cmake/README.md
              '-D PKG_KOKKOS=on',
              '-D PKG_GPU=off',
              '-D PKG_MPIIO=on',
              '-D PKG_USER-REAXC=on',
              '-D PKG_KSPACE=on',
              '-D PKG_MOLECULE=on',
              '-D PKG_REPLICA=on',
              '-D PKG_RIGID=on',
              '-D PKG_MISC=on',
              '-D PKG_MANYBODY=on',
              '-D PKG_ASPHERE=on',
              ],
  directory='{}/cmake'.format(srcdir),
  # Force CUDA dynamic linking, see
  # https://github.com/openucx/ucx/wiki/NVIDIA-GPU-Support
  preconfigure=['sed -i \'s/^cuda_args=""/cuda_args="--cudart shared"/g\' {}/lib/kokkos/bin/nvcc_wrapper'.format(srcdir)],
  prefix='/usr/local/lammps-{}'.format(compute_capability),
  url='https://github.com/lammps/lammps/archive/{}.tar.gz'.format(twcc_lammps))
# ----------------------------------------------------------------------------------------


########
# Runtime stage (Stage 1)
########

Stage1 += baseimage(image=USERARG.get('runtime_image', default_runtime_image))

# replace mirror site to NCHC free software labs
Stage1 += raw(singularity="sed -i 's#archive.ubuntu.com/ubuntu/#free.nchc.org.tw/ubuntu#' /etc/apt/sources.list")

# Build stage runtime support
Stage1 += Stage0.runtime()

########
# OpenMPI envs
########
Stage1 += environment(variables={
# enable openmpi-cuda Point-to-point management layer(pml) over ucx-cuda
# ref : https://www.open-mpi.org/faq/?category=runcuda#run-ompi-cuda-ucx
                                 'OMPI_MCA_pml': 'ucx',
                                 'UCX_TLS': 'rc,sm,cuda_copy,gdr_copy,cuda_ipc',
# for avoid this ucx-cuda issue
# ref : https://github.com/openucx/ucx/wiki/NVIDIA-GPU-Support
                                 'UCX_MEMTYPE_CACHE': 'n'
                                })
# kmo try to build lammps
# base on https://github.com/NVIDIA/hpc-container-maker/blob/master/recipes/lammps/lammps.py
# need to verify
# ------------------------------
########
# LAMMPS envs
########
Stage1 += environment(variables=
  {
  'LD_LIBRARY_PATH': '/usr/local/lammps-{}/lib:$LD_LIBRARY_PATH'.format(
    compute_capability),
  'PATH': '/usr/local/lammps-{}/bin:$PATH'.format(compute_capability),
  })
# ------------------------------
