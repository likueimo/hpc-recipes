########
#  --------------------------------
#  apps
cntr_cmake = '3.17.3'
cntr_lammps = 'patch_15Jun2020'
#  --------------------------------

# kmo try to build lammps
# base on https://github.com/NVIDIA/hpc-container-maker/blob/master/recipes/lammps/lammps.py
# need to verify
# -----------------------------------------------------------------------------------
########
# LAMMPS
########

# lammps build by cmake
Stage0 += cmake(eula=True, version=cntr_cmake)

gpu_arch = USERARG.get('gpu_arch', 'Volta70')

compute_capability = 'sm' + gpu_arch[-2:]
srcdir = '/var/tmp/lammps-{}'.format(cntr_lammps)

Stage0 += comment('LAMMPS version {0} for CUDA compute capability {1}'.format(
  cntr_lammps, compute_capability))

# LAMMPS dependencies
Stage0 += apt_get(ospackages=['bc', 'git', 'libgomp1', 'libhwloc-dev', 'make',
                              'tar', 'wget'])

# LAMMPS build
Stage0 += generic_cmake(
  build_directory='{0}/build-{1}'.format(srcdir, gpu_arch),
  cmake_opts=[
              # cmake
              #'-D CUDA_USE_STATIC_CUDA_RUNTIME=off',
              '-D MPI_C_COMPILER=mpicc',
              # https://lammps.sandia.gov/doc/Build_cmake.html
              #'-D CMAKE_BUILD_TYPE=Release',
              # https://lammps.sandia.gov/doc/Build_basics.html#serial
              '-D BUILD_MPI=yes',
              '-D BUILD_OMP=yes',
              # https://lammps.sandia.gov/doc/Build_basics.html#exe
              #'-D BUILD_LIB=no',
              '-D BUILD_SHARED_LIBS=yes',
              # https://lammps.sandia.gov/doc/Build_extras.html#kokkos
              #'-D KOKKOS_ARCH="{}"'.format(gpu_arch),
              '-D Kokkos_ARCH_SKX=yes',
              '-D Kokkos_ARCH_VOLTA70=yes',
              '-D Kokkos_ENABLE_CUDA=yes',
              '-D Kokkos_ENABLE_OPENMP=yes',
              '-D Kokkos_ENABLE_HWLOC=yes',
              '-D CMAKE_CXX_COMPILER={}/lib/kokkos/bin/nvcc_wrapper'.format(srcdir),
              # https://github.com/lammps/lammps/blob/master/cmake/README.md
              '-D PKG_KOKKOS=on',
              '-D PKG_GPU=off',
              '-D PKG_MPIIO=on',
              '-D PKG_USER-REAXC=on',
              '-D PKG_KSPACE=on',
              '-D PKG_MOLECULE=on',
              '-D PKG_REPLICA=on',
              '-D PKG_RIGID=on',
              '-D PKG_MISC=on',
              '-D PKG_MANYBODY=on',
              '-D PKG_ASPHERE=on',
              ],
  directory='{}/cmake'.format(srcdir),
  # Force CUDA dynamic linking, see
  # https://github.com/openucx/ucx/wiki/NVIDIA-GPU-Support
  preconfigure=['sed -i \'s/^cuda_args=""/cuda_args="--cudart shared"/g\' {}/lib/kokkos/bin/nvcc_wrapper'.format(srcdir)],
  prefix='/usr/local/lammps-{}'.format(compute_capability),
  url='https://github.com/lammps/lammps/archive/{}.tar.gz'.format(cntr_lammps))
# ----------------------------------------------------------------------------------------

########
# LAMMPS envs
########
Stage0 += environment(variables=
  {
  'LD_LIBRARY_PATH': '/usr/local/lammps-{}/lib:$LD_LIBRARY_PATH'.format(
    compute_capability),
  'PATH': '/usr/local/lammps-{}/bin:$PATH'.format(compute_capability),
  })
# ------------------------------
