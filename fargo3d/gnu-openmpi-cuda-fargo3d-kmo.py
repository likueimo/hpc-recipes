#!/usr/bin/env python
########
#  optimized OpenMPI-CUDA singularity recipe for TWCC
#  update : 2020/03/16
#
#  host : TWCC GPU NODE
#  --------------------------------
#  CentOS 7.5 x86_64
#  mlnx_ofed (4.4-2.0.7.0/4.4-2.0.7.0.3)
#  gdrcopy (2.0)
#  knem (1.1.3)
#  slurm/pmix (18.08.8/2.2.2)
#
#  TO DO (HOST):
#  xpmem (waiting for host install xpmem kernel module)
#  hcoll (waiting for host enable sharp service)
#  --------------------------------
#
#  guest : singularity container
#  --------------------------------
#  guest mlnx_ofed 4.x userspace driver,
#  should be compatible host mlnx_ofed 4.4
twcc_mlnx_ofed = '4.7-3.2.9.0'

#  guest gdrcopy/knem userspace library version, slurm pmi2 version,
#  have to same as host gdrcopy/knem/slurm version
twcc_gdrcopy = '2.0'
twcc_knem = '1.1.3'
twcc_slurm_pmi2 = '18.08.8'

#  ucx, the newer the better
twcc_ucx = '1.7.0'

#　openmpi, if no MPI API compatibility issue
#  the newer the better
twcc_openmpi = '4.0.3'

#  TO DO (GUEST) :
#  enhance pmix compatibility host slurm plugin <-> guest
#  From Mellanox HPC-X docs :
#  intended to be used with SLURM PMIx plugin,
#  Open MPI should be build against \
#  external PMIx, Libevent and HWLOC \
#  and the same Libevent and PMIx libraries \
#  should be used for both SLURM and Open MPI
#  --------------------------------
########

import hpccm

# Use appropriate container base images based on the CPU architecture
arch = 'x86_64'
default_build_image = 'nvidia/cuda:10.1-devel-ubuntu18.04'
default_runtime_image = 'nvidia/cuda:10.1-base-ubuntu18.04'

########
# Build stage (Stage 0)
########

# Base image
Stage0 += baseimage(image=USERARG.get('build_image', default_build_image),
                    _arch=arch, _as='build')

# replace mirror site to NCHC free software labs
Stage0 += raw(singularity="sed -i 's#archive.ubuntu.com/ubuntu/#free.nchc.org.tw/ubuntu#' /etc/apt/sources.list")

# enbale gfortran
Stage0 += gnu(fortran=True)

# OpenMPI Communication stack:
#
# OpenMPI 4 (built in pmix 3 -> enable 'srun --mpi=pmix')
# with
# Slurm PMI2 (enable 'srun --mpi=pmi2')
# UCX (with KNEM + GDRCOPY + Mellanox OFED)

Stage0 += mlnx_ofed(version=twcc_mlnx_ofed)

Stage0 += gdrcopy(ldconfig=True, version=twcc_gdrcopy)

Stage0 += knem(ldconfig=True, version=twcc_knem)

Stage0 += ucx(
              ofed=True,
              without_java=True,
              enable_devel_headers=True,
              ldconfig=True,
              version=twcc_ucx,
              knem='/usr/local/knem',
              #xpmem='' wait for host enable xpmem kernel module
              gdrcopy='/usr/local/gdrcopy',
             )

# with Slurm PMI2
Stage0 += slurm_pmi2(prefix='/usr/local/slurm-pmi2', version=twcc_slurm_pmi2)

mpi = openmpi(
              cuda=True,
# becasue of https://www.open-mpi.org/faq/?category=openfabrics#ofa-device-error
# need to disable verbs for openmpi-cuda with ucx -> then infiniband=False
              infiniband=False,
              #with_hcoll=True, wait for host enable sharp
              without_xpmem=True,
              enable_mpi_cxx=True,
              enable_mpi_fortran=True,
              enable_mpi1_compatibility=True,
              ldconfig=True,
              version=twcc_openmpi,
              ucx='/usr/local/ucx',
              with_pmi='/usr/local/slurm-pmi2',
              with_platform='contrib/platform/mellanox/optimized',
             )

Stage0 += mpi


# build fargo3d
Stage0 += packages(apt=['python'])
Stage0 += generic_build(build=[
                               'make \
                                PARALLEL=1 \
                                GPU=1 \
                                MPICUDA=1 \
                                FARGO_ARCH=TESLAOPENMPI \
                                CUDAOPT_TESLAOPENMPI=" -O3 -arch=sm_70"'
                               ],
                        install=['mkdir -p /usr/local/fargo3d/bin',
                                 'cp /var/tmp/public/fargo3d /usr/local/fargo3d/bin'],
                        prefix='/usr/local/fargo3d/bin',
                        repository='https://bitbucket.org/fargo3d/public.git')
Stage0 += environment(variables={'PATH': '/usr/local/fargo3d/bin:$PATH'})



########
# Runtime stage (Stage 1)
########

Stage1 += baseimage(image=USERARG.get('runtime_image', default_runtime_image))

# replace mirror site to NCHC free software labs
Stage1 += raw(singularity="sed -i 's#archive.ubuntu.com/ubuntu/#free.nchc.org.tw/ubuntu#' /etc/apt/sources.list")

# Build stage runtime support
Stage1 += Stage0.runtime()

########
# OpenMPI envs
########
Stage1 += environment(variables={
# enable openmpi-cuda Point-to-point management layer(pml) over ucx-cuda
# ref : https://www.open-mpi.org/faq/?category=runcuda#run-ompi-cuda-ucx
                                 'OMPI_MCA_pml': 'ucx',
                                 'UCX_TLS': 'rc,sm,cuda_copy,gdr_copy,cuda_ipc',
# for avoid this ucx-cuda issue
# ref : https://github.com/openucx/ucx/wiki/NVIDIA-GPU-Support
                                 'UCX_MEMTYPE_CACHE': 'n'
                                })
########
# fargo3d envs
########
Stage1 += packages(apt=['python'])
Stage1 += packages(apt=['make'])
Stage1 += environment(variables={'PATH': '/usr/local/fargo3d/bin:$PATH'})
