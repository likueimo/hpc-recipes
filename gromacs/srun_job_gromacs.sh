#!/bin/bash
#SBATCH --job-name=Hello_gromacs  ## job name
#SBATCH --nodes=1                 ## 索取 1 節點
#SBATCH --ntasks-per-node=8       ## 每個節點跑 8 個 srun tasks
#SBATCH --cpus-per-task=4         ## 每個 srun task 用 4 CPUs
#SBATCH --gres=gpu:8              ## 每個節點索取 8 GPUs
#SBATCH --time=00:10:00           ## 最長跑 10 分鐘 (測試完這邊記得改掉，或是測試完直接刪除該行)
#SBATCH --account="GOV108029"     ## iService_ID 請填入計畫ID(ex: MST108XXX)，扣款也會根據此計畫ID
#SBATCH --partition=gtest         ## gtest 為測試用 queue，後續測試完可改 gp1d(最長跑1天)、gp2d(最長跑2天)、p4d(最長跑4天)

# this script modify from 
# https://gitlab.com/NVHPC/ngc-examples/-/blob/master/lammps/24Oct2018/multi-node/slurm/srun.slurm

# Load required modules
module load singularity

# Script arguments
GPU_COUNT=8
SIF=gnu-mt-openmpi-cuda-gromacs.sif

# Singularity will mount the host PWD to /host_pwd in the container
SINGULARITY="singularity run --nv ${SIF}"


# Prepare benchmark data
${SINGULARITY} gmx_mpi grompp -f pme.mdp

# Run benchmark
srun --mpi=pmi2 ${SINGULARITY} gmx_mpi mdrun \
                 -ntmpi ${GPU_COUNT} \
                 -nb gpu \
                 #-ntomp $SLURM_CPUS_PER_TASK \
                 #-pin on \
                 -v \
                 -noconfout \
                 -nsteps 5000 \
                 -s topol.tpr
