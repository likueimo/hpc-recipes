#!/usr/bin/env python
########
#  optimized Multi-Threaded OpenMPI-CUDA gromacs singularity recipe for TWCC
#  update : 2020/03/16
#
#  host : TWCC GPU NODE
#  --------------------------------
#  CentOS 7.5 x86_64
#  mlnx_ofed (4.4-2.0.7.0/4.4-2.0.7.0.3)
#  gdrcopy (2.0)
#  knem (1.1.3)
#  slurm/pmix (18.08.8/2.2.2)
#
#  TO DO (HOST):
#  xpmem (waiting for host install xpmem kernel module)
#  hcoll (waiting for host enable sharp service)
#  --------------------------------
#
#  guest : singularity container
#  --------------------------------
#  guest mlnx_ofed 4.x userspace driver,
#  should be compatible host mlnx_ofed 4.4
twcc_mlnx_ofed = '4.7-3.2.9.0'

#  guest gdrcopy/knem userspace library version, slurm pmi2 version,
#  have to same as host gdrcopy/knem/slurm version
twcc_gdrcopy = '2.0'
twcc_knem = '1.1.3'
twcc_slurm_pmi2 = '18.08.8'

#  ucx, the newer the better
twcc_ucx = '1.7.0'

#　openmpi, if no MPI API compatibility issue
#  the newer the better
twcc_openmpi = '4.0.3'

#  TO DO (GUEST) :
#  enhance pmix compatibility host slurm plugin <-> guest
#  From Mellanox HPC-X docs :
#  intended to be used with SLURM PMIx plugin,
#  Open MPI should be build against \
#  external PMIx, Libevent and HWLOC \
#  and the same Libevent and PMIx libraries \
#  should be used for both SLURM and Open MPI
#  --------------------------------
########
# apps
twcc_cmake = '3.16.5'
twcc_gromacs = '2020.1'
#  --------------------------------



import hpccm

# Use appropriate container base images based on the CPU architecture
arch = 'x86_64'
default_build_image = 'nvidia/cuda:10.1-devel-ubuntu18.04'
default_runtime_image = 'nvidia/cuda:10.1-base-ubuntu18.04'

########
# Build stage (Stage 0)
########

# Base image
Stage0 += baseimage(image=USERARG.get('build_image', default_build_image),
                    _arch=arch, _as='build')

# replace mirror site to NCHC free software labs
Stage0 += raw(singularity="sed -i 's#archive.ubuntu.com/ubuntu/#free.nchc.org.tw/ubuntu#' /etc/apt/sources.list")

# enbale gfortran
Stage0 += gnu(fortran=True)

# OpenMPI Communication stack:
#
# OpenMPI 4 (built in pmix 3 -> enable 'srun --mpi=pmix')
# with
# Slurm PMI2 (enable 'srun --mpi=pmi2')
# UCX (with KNEM + GDRCOPY + Mellanox OFED)

Stage0 += mlnx_ofed(version=twcc_mlnx_ofed)

Stage0 += gdrcopy(ldconfig=True, version=twcc_gdrcopy)

Stage0 += knem(ldconfig=True, version=twcc_knem)

Stage0 += ucx(
              ofed=True,
              without_java=True,
              # for Multi-Threaded
              enable_mt=True,
              enable_devel_headers=True,
              ldconfig=True,
              version=twcc_ucx,
              knem='/usr/local/knem',
              #xpmem='' wait for host enable xpmem kernel module
              gdrcopy='/usr/local/gdrcopy',
             )

# with Slurm PMI2
Stage0 += slurm_pmi2(prefix='/usr/local/slurm-pmi2', version=twcc_slurm_pmi2)

mpi = openmpi(
              cuda=True,
# becasue of https://www.open-mpi.org/faq/?category=openfabrics#ofa-device-error
# need to disable verbs for openmpi-cuda with ucx -> then infiniband=False
              infiniband=False,
              #with_hcoll=True, wait for host enable sharp
              without_xpmem=True,
              enable_mpi_cxx=True,
              enable_mpi_fortran=True,
              enable_mpi1_compatibility=True,
              ldconfig=True,
              version=twcc_openmpi,
              ucx='/usr/local/ucx',
              with_pmi='/usr/local/slurm-pmi2',
              with_platform='contrib/platform/mellanox/optimized',
             )

Stage0 += mpi

# kmo try to build gromacs
# base on https://github.com/NVIDIA/hpc-container-maker/blob/master/recipes/gromacs/gromacs.py
# need to verify
# -----------------------------------------------------------------------------------
########
# GROMACS
########

# gromacs build by cmake
Stage0 += cmake(eula=True, version=twcc_cmake)

Stage0 += generic_cmake(cmake_opts=['-D CMAKE_BUILD_TYPE=Release',
                                    '-D CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda',
                                    '-D GMX_BUILD_OWN_FFTW=ON',
                                    '-D GMX_GPU=ON',
                                    '-D GMX_MPI=ON',
                                    '-D GMX_OPENMP=ON',
                                    '-D GMX_PREFER_STATIC_LIBS=ON',
                                    '-D MPIEXEC_PREFLAGS=--allow-run-as-root'],
                        prefix='/usr/local/gromacs',
                        url='http://ftp.gromacs.org/pub/gromacs/gromacs-{}.tar.gz'.format(twcc_gromacs))

Stage0 += environment(variables={'PATH': '$PATH:/usr/local/gromacs/bin'})

Stage0 += label(metadata={'gromacs.version': twcc_gromacs})

# -----------------------------------------------------------------------------------



########
# Runtime stage (Stage 1)
########

Stage1 += baseimage(image=USERARG.get('runtime_image', default_runtime_image))

# replace mirror site to NCHC free software labs
Stage1 += raw(singularity="sed -i 's#archive.ubuntu.com/ubuntu/#free.nchc.org.tw/ubuntu#' /etc/apt/sources.list")

# Build stage runtime support
Stage1 += Stage0.runtime()

########
# OpenMPI envs
########
Stage1 += environment(variables={
# enable openmpi-cuda Point-to-point management layer(pml) over ucx-cuda
# ref : https://www.open-mpi.org/faq/?category=runcuda#run-ompi-cuda-ucx
                                 'OMPI_MCA_pml': 'ucx',
                                 'UCX_TLS': 'rc,sm,cuda_copy,gdr_copy,cuda_ipc',
# for avoid this ucx-cuda issue
# ref : https://github.com/openucx/ucx/wiki/NVIDIA-GPU-Support
                                 'UCX_MEMTYPE_CACHE': 'n'
                                })
# kmo try to build gromacs
# base on https://github.com/NVIDIA/hpc-container-maker/blob/master/recipes/gromacs/gromacs.py
# need to verify
# -----------------------------------------------------------------------------------
########
# GROMACS envs
########

Stage1 += packages(ospackages=['cuda-cufft-10-1'])

Stage1 += environment(variables={'PATH': '$PATH:/usr/local/gromacs/bin'})

Stage1 += label(metadata={'gromacs.version': twcc_gromacs})

# -----------------------------------------------------------------------------------
